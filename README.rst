==========================================
SageMath interface to the GAP package GBNP
==========================================

This module is an interface to the `GBNP
<https://gap-packages.github.io/gbnp/>`_ package, which provides algorithms for
computing Groebner bases of noncommutative polynomials with coefficients from a
field implemented in GAP and with respect to the "total degree first then
lexicographical" ordering.

UPDATE: Latest version is at `SageMath trac ticket #31446 <https://trac.sagemath.org/ticket/31446>`_.

Installation
------------

At least on Linux, we first download the tarball::

    $ wget https://github.com/gap-packages/gbnp/archive/refs/tags/v1.0.4.tar.gz

and verify integrity::

    $ sha256sum v1.0.4.tar.gz
    453897eb66de3a2537ef913d43d499cd80e5591ca79ab59dbca6dfeb17d26254  v1.0.4.tar.gz

Unpack the tarball into the ``pkg`` directory::

    $ sage -sh
    (sage-sh) $ cd $SAGE_ROOT/local/share/gap/pkg/
    (sage-sh) $ tar -xvf /the/path/to/gbnp/v1.0.4.tar.gz
    (sage-sh) $ exit

To check that the package installed correctly, try loading it::

   $ sage
   sage: gap.LoadPackage('"gbnp"')
   true

Usage
-----

TBD. For now, see the docstrings in `gbnp.py <gbnp.py>`_.

Links
-----
* https://gap-packages.github.io/gbnp and `documentation
  <https://gap-packages.github.io/gbnp/doc/chap0.html>`_. The old package page
  is `here <https://www.gap-system.org/Packages/gbnp.html>`_

* https://github.com/gap-packages/gbnp

* Arjeh M. Cohen and Dié .A.H. Gijsbers. Noncommutative groebner basis
  computations. Report, 2003, http://www.win.tue.nl/~amc/pub/grobner/gbnp.pdf

* Jan Willem Knopper. GBNP and Vector Enumeration. Internship report, 2004
  http://mathdox.org/gbnp/knopper.pdf

License
-------

See the `LICENSE <LICENSE>`_ file. This programs is released under
GPL-3.0-or-later:

    Copyright 2019, 2020, 2021 Guy Blachar and Tomer Bauer.

    This GBNP interface is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program.  If not, see <https://www.gnu.org/licenses/>.
